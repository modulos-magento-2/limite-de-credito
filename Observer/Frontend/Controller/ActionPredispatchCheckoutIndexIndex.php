<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Eparts\LimiteCredito\Observer\Frontend\Controller;

class ActionPredispatchCheckoutIndexIndex implements \Magento\Framework\Event\ObserverInterface
{
	/**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var RedirectInterface
     */
    protected $redirect;
	
	protected $customerSession;


    /**
     * @param ManagerInterface $messageManager
     * @param RedirectInterface $redirect
     * @param CustomerCart $cart
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
		\Magento\Customer\Model\Session $customerSession
    ) {
        $this->messageManager = $messageManager;
        $this->redirect = $redirect;
		$this->customerSession = $customerSession;
    }
    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $controller = $observer->getControllerAction();
		

		if ($this->customerSession->getCustomer()->getLimiteCredito() == 'X') {
            $this->messageManager->addErrorMessage(
                __('Voce está sem limite de crédito, entre em contato com o nosso setor financeiro para que possa liberar o seu crédito e concluir sua compra')
            );
            $this->redirect->redirect($controller->getResponse(), 'checkout/cart');
        }
    }
}

