<?php

namespace Eparts\LimiteCredito\Observer\Frontend;

use Magento\Framework\Event\Observer;
use Magento\Sales\Model\Order;


class AfterPlaceOrder implements \Magento\Framework\Event\ObserverInterface
{
    const STATUS_PENDING_APPROVAL = 'pending_approval';

    protected $customerSession;

    public function __construct(\Magento\Customer\Model\Session $customerSession)
    {
        $this->customerSession = $customerSession;
    }

    /**
     * @param Observer $observer
     * @return bool
     */
    public function execute(Observer $observer)
    {

        $order = $observer->getEvent()->getOrder();

        $totalOrder = (float) $order->getGrandTotal();
        $totalLimite = (float) $this->customerSession->getCustomer()->getLimiteDisponivel();

        if ($totalLimite <  $totalOrder) {

            if ($order->getStatus() == self::STATUS_PENDING_APPROVAL) {
                return true;
            }

            $order->setState(Order::STATE_HOLDED)->setStatus(self::STATUS_PENDING_APPROVAL);
            $order->save();
        }

    }

}