<?php

namespace Eparts\LimiteCredito\Model;


class PostManagement {

    protected $_customerFactory;
    protected $_customer;

    public function __construct(
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Customer $customers
    ) {
        $this->_customerFactory = $customerFactory;
        $this->_customer = $customers;
    }

    /**
     * {@inheritdoc}
     */
    public function setLimitCredit($limiteTotal, $limiteUtilizado, $cnpj)
    {
        try {
            $id = $this->getFilteredCustomerCollection($cnpj);

            if (!$id) {
                return json_encode(['success' => false, 'message' => 'Usuário naão localizado.']);
            }

            $porcentagemLimite = $limiteUtilizado / 100;
            $limiteUtilizadoFinal = $limiteTotal * $porcentagemLimite;
            $limiteDisponivel = bcsub($limiteTotal, $limiteUtilizadoFinal, 2);

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
            $customer = $customerFactory->load($id);
            $customer->setLimiteCredito($this->formatDecimal($limiteTotal))
                ->setLimiteUtilizado($this->formatDecimal($limiteUtilizadoFinal))
                ->setLimiteDisponivel($this->formatDecimal($limiteDisponivel))
                ->save();

            $response = [
                'success' => true,
                'message' => 'Limites atualizados com sucesso'
            ];

        } catch (\Exception $e) {
            $response = ['success' => false, 'message' => $e->getMessage()];
        }

        return json_encode($response);
    }

    protected function formatDecimal($value)
    {
        return number_format($value,2,'.','');
    }


    /**
     * @param $taxvat
     * @return mixed
     */
    protected function getFilteredCustomerCollection($taxvat) {

        $customer = $this->_customerFactory->create()->getCollection()
                ->addAttributeToSelect("entity_id")
                ->addAttributeToFilter("taxvat", array('taxvat' => $taxvat))->load();

        if (!empty($customer->getData())) {
            return $customer->getData()[0]['entity_id'];
        }

        return false;
    }
}
