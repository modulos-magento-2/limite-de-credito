<?php

namespace Eparts\LimiteCredito\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\StatusFactory;
use Magento\Sales\Model\ResourceModel\Order\StatusFactory as StatusResourceFactory;


class UpgradeSchema implements UpgradeSchemaInterface
{

    const ORDER_STATUS_PROCESSING_FULFILLMENT_CODE = 'pending_approval';
    const ORDER_STATUS_PROCESSING_FULFILLMENT_LABEL = 'Pedido pendente de aprovação';

    protected $statusFactory;
    protected $statusResourceFactory;


    public function __construct(
        StatusFactory $statusFactory,
        StatusResourceFactory $statusResourceFactory
    ) {
        $this->statusFactory = $statusFactory;
        $this->statusResourceFactory = $statusResourceFactory;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '0.0.2') < 0) {
            $this->addNewOrderProcessingStatus();
        }

        $installer->endSetup();
    }

    /**
     * @throws \Exception
     */
    protected function addNewOrderProcessingStatus()
    {
        $statusResource = $this->statusResourceFactory->create();
        $status = $this->statusFactory->create();
        $status->setData([
            'status' => self::ORDER_STATUS_PROCESSING_FULFILLMENT_CODE,
            'label' => self::ORDER_STATUS_PROCESSING_FULFILLMENT_LABEL,
        ]);
        try {
            $statusResource->save($status);
        } catch (\Exception $exception) {
            return;
        }

        $status->assignState(Order::STATE_HOLDED, false, true);
    }
}
