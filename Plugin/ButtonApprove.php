<?php

namespace Eparts\LimiteCredito\Plugin;

use Magento\Sales\Block\Adminhtml\Order\View as OrderView;

class ButtonApprove
{
    protected $orderRepository;
    protected $request;

    /**
     * @param OrderView $subject
     */
    public function beforeSetLayout(OrderView $subject)
    {
        $status = $subject->getOrder()->getStatus();

        if ($status != 'pending_approval') {
            $subject->removeButton('order_custom_button');
        } else {
            $subject->addButton(
                'order_custom_button',
                [
                    'label' => __('Aprovar Pedido'),
                    'id' => 'order-view-approve-order-button',
                    'onclick' => 'setLocation(\'' . $subject->getUrl('limitecredito/updateOrder/index') . '\')'
                ]
            );
        }
    }
}