<?php

namespace Eparts\LimiteCredito\Controller\Adminhtml\UpdateOrder;
use Magento\Sales\Model\Order;


class Index extends \Magento\Sales\Controller\Adminhtml\Order
{
    protected $resultPageFactory;

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $order = $this->_initOrder();
        if ($order) {
            try {
                $order->setStatus('pending')
                    ->setState(Order::STATE_NEW)
                    ->save();

                $this->messageManager->addSuccessMessage('Pedido liberado com sucesso');

            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }

            return $this->resultRedirectFactory->create()->setPath(
                'sales/order/view',
                ['order_id' => $order->getEntityId()]
            );
        }

        return $this->resultRedirectFactory->create()->setPath('sales/*/');
    }
}
