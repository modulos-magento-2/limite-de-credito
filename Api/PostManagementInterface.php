<?php
namespace Eparts\LimiteCredito\Api;


interface PostManagementInterface {


    /**
     *
     * @param float limiteTotal
     * @param float limiteUtilizado
     * @param string $cnpj
     * @return string
     */
    public function setLimitCredit($limiteTotal, $limiteUtilizado, $cnpj);
}